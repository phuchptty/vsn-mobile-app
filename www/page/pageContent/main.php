<?php
$sql = $userql -> query("SELECT * FROM `authme` WHERE `realname` = '".$_SESSION['user']."' ");
$user  = $sql -> fetch_array(MYSQLI_ASSOC);

$sql = $skinql -> query("SELECT * FROM `players` WHERE `Nick` = '".$user['username']."'");
$skin = $sql -> fetch_array(MYSQLI_ASSOC);
?>
<div class="container">
	<div class="section">
		<div class="row">
			<div class="col s6">
			
				<div class="card blue darken-1">
				<div class="card-content white-text">
					
					<table class="bordered responsive-table">
						<thead>
							<tr>
								<th></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Skin Head</td>
								<td>
									<?php 

									
										$data = curl_download("https://api.mojang.com/users/profiles/minecraft/".$skin['Skin']);
										$data = json_decode($data,true);
										//print_r($data);
										if (isset($data['error']) && $data['error'] == "Not Found"){
											$url = "https://i.pinimg.com/originals/71/21/30/712130935037791327290d41e2866e0a.jpg";
											$skin = "https://lh3.googleusercontent.com/kcEh6LtwvYN1dUrh1d-ctvtFLbkVdT6ba-8Tr7ePYz6FCmHcuTA5K14Sm1CgEbuKHuqI-gWlifb7XdEKlG2zTw";
										}else{
											$uuid = $data['id'];
											$url = "https://crafatar.com/avatars/".$uuid."?overlay&size=50&default=MHF_Steve";
											$skin = "";
										}
										

									?>
									<img src="<?=$url;?>" <?php if (isset($data['error']) && $data['error'] == "Not Found"){?> style="width: 10%;" <?php } ?> alt="player head">
								</td>
							</tr>
							<tr>
								<td>Tên In-game</td>
								<td><?=$user['realname'];?></td>
							</tr>
							<tr>
								<td>Username</td>
								<td><?=$user['username'];?></td>
							</tr>
							<tr>
								<td>Email</td>
								<td><?=$user['email'];?></td>
							</tr>
							<tr>
								<td>Đăng Nhập Lần Cuối</td>
								<td><?=date('d/m/Y h:i:s',$user['lastlogin']/1000);?></td>
							</tr>
							<tr>
								<td>IP Đăng Nhập</td>
								<td><?=$user['ip'];?></td>
							</tr>
						</tbody>
					</table>
				</div>
				</div>

			</div>

			<div class="col s6" style="text-align: center">
				<div class="card pink accent-2">
				<div class="card-content white-text">
					<table class="bordered responsive-table">
						<thead>
							<tr>
								<th>Skin không Overlay</th>
								<th>Skin với Overlay</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<?php
										if ($skin == ""){
										?>
											<img src="https://crafatar.com/renders/body/<?=$uuid;?>?default=MHF_Steve" alt="Non-overlay">
									<?php 

										}else{
											?>
											<img src="<?=$skin;?>" <?php if (isset($data['error']) && $data['error'] == "Not Found"){?> style="width: 50%;" <?php } ?> >
									<?php
										} 
									?>
								</td>

								<td>
									<?php
										if ($skin == ""){
										?>
											<img src="https://crafatar.com/renders/body/<?=$uuid;?>?default=MHF_Steve&overlay" alt="Non-overlay">
									<?php 

										}else{
											?>
											<img src="<?=$skin;?>" <?php if (isset($data['error']) && $data['error'] == "Not Found"){?> style="width: 50%;" <?php } ?> >
									<?php
										} 
									?>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Floating Action Button -->
	<div class="fixed-action-btn " style="bottom: 50px; right: 19px;">
		<a class="btn-floating btn-large gradient-45deg-light-blue-cyan gradient-shadow">
			<i class="material-icons">add</i>
		</a>
			
		<ul>
			<li>
					<a href="css-helpers.html" class="btn-floating blue">
					<i class="material-icons">help_outline</i>
					</a>
			</li>
				
			<li>
					<a href="cards-extended.html" class="btn-floating green">
					<i class="material-icons">widgets</i>
					</a>
			</li>
				
			<li>
					<a href="app-calendar.html" class="btn-floating amber">
					<i class="material-icons">today</i>
					</a>
			</li>
				
			<li>
					<a href="app-email.html" class="btn-floating red">
					<i class="material-icons">mail_outline</i>
					</a>
			</li>
		</ul>
	</div>

</div>
