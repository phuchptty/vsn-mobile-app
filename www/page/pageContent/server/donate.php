<div class="container">

	<div class="row">
		<div class="col s6">
			<div class="card light-green lighten-3">

			<div class="card-header black-text">
				<center><h4>Thẻ Cào</h4></center>
			</div>

			<div class="card-content black-text">
				<div class="row">
					<form class="col s12">
						<div class="row">

							<div class="col s12">
								<div class="row">
									<div class="col s6">
										<p>
											<input name="loaithe" type="radio" id="Viettel" />
											<label for="Viettel">Viettel</label>
										</p>

										<p>
											<input name="loaithe" type="radio" id="Vinaphone" disabled/>
											<label for="Vinaphone">Vinaphone</label>
										</p>

										<p>
											<input name="loaithe" type="radio" id="Mobifone" />
											<label for="Mobifone">Mobifone</label>
										</p>

										<p>
											<input name="loaithe" type="radio" id="gate" disabled/>
											<label for="gate">FPT Gate</label>
										</p>

										<p>
											<input name="loaithe" type="radio" id="Playduo" />
											<label for="Playduo">Playduo</label>
										</p>

										<p>
											<input name="loaithe" type="radio" id="Garena" disabled/>
											<label for="Garena">Garena</label>
										</p>
									</div>

									<div class="col s6">
										<select>
											<option value="" disabled selected>Chọn Mệnh Giá</option>
											<option value="10">10.000</option>
											<option value="20">20.000</option>
											<option value="50">50.000</option>
											<option value="100">100.000</option>
											<option value="200">200.000</option>
											<option value="500">500.000</option>
										</select>
										<label>Mệnh Giá Thẻ</label>
									</div>

								</div>
							</div>

							<div class="input-field col s12">
								<input id="code" type="text" class="validate">
								<label for="code">Mã Thẻ</label>
							</div>

							<div class="input-field col s12">
								<input id="seri" type="text" class="validate">
								<label for="seri">Số Seri</label>
							</div>

							<div class="col s12">
								<h5>Lưu Ý: Chọn Sai Mệnh Giá Có Thể Dẫn Đến Mất Thẻ. BQT Không Chịu Trách Nhiệm</h5>
							</div>

							<div class="input-field col s12 text-center">
								<a class="btn waves-effect waves-light" id="send-btn" name="send" onclick="cardSend()">Ủng Hộ
									<i class="material-icons right">send</i>
								</a>
							</div>

						</div>
					</form>
				</div>

			</div>
			</div>
		</div>

		<div class="col s6">
			<div class="card blue darken-3">
			
			<div class="card-header white-text">
				<center><h4>Paypal</h4></center>
			</div>
			
			<div class="card-content white-text">
				<div class="row">
					<div class="input-field col s12">
						<input id="paypal-money" type="text" class="validate">
						<label for="paypal-money">Số Tiền Ủng Hộ (US Dollar)</label>
					</div>
					<div class="col s12 text-center">
						<a class="btn waves-effect waves-light" id="send-btn" name="send" onclick="paypalSend()">Ủng Hộ Quay Paypal
							<i class="material-icons right">send</i>
						</a>
					</div>
				</div>
			</div>
			
			</div>
		</div>

	</div>

	<div class="row">

		<div class="col s4">
			<div class="card blue darken-3">
			
			<div class="card-header white-text">
				<center><h4>Viettel Pay</h4></center>
			</div>
			
			<div class="card-content white-text">
				<div class="row">
					<div class="col s12">
						<p>StreamLab: </p>
						<p>Ủng Hộ Tôi: </p>
						<p>Playduo: </p>
					</div>
				</div>
			</div>

			</div>
		</div>

		<div class="col s4">
			<div class="card blue darken-3">
			
			<div class="card-header white-text">
				<center><h4>Các Phương Thức Khác</h4></center>
			</div>
			
			<div class="card-content white-text">
				<div class="row">
					<div class="col s12">
						<p>StreamLab: </p>
						<p>Ủng Hộ Tôi: </p>
						<p>Playduo: </p>
					</div>
				</div>
			</div>

			</div>
		</div>

		<div class="col s4">
			<div class="card blue darken-3">
			
			<div class="card-header white-text">
				<center>
					<h4>Thu Tiền Tận Nhà</h4>
					<h4>Nộp Tiền Tại Văn Phòng</h4>
				</center>
			</div>
			
			<div class="card-content white-text">
				<div class="row">
					<div class="col s12">
						<p>StreamLab: </p>
						<p>Ủng Hộ Tôi: </p>
						<p>Playduo: </p>
					</div>
				</div>
			</div>

			</div>
		</div>
	</div>

</div>