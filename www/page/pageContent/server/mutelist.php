<div class="container">
<div class="card deep-purple lighten-2">
<div class="card-content white-text">

<?php
	$lbql = new Mysqli("localhost","root","","litebans");

	if ($userql->connect_errno) {
    	die('Connect Server Litebans Error: ' . $db->connect_errno);
    }

	$result = $lbql -> query('select count(id) as total from `litebans_mutes`');
	$row = mysqli_fetch_assoc($result);
	$total_records = $row['total'];
	 
	// BƯỚC 3: TÌM LIMIT VÀ CURRENT_PAGE
	$current_page = isset($_GET['page']) ? $_GET['page'] : 1;
	$limit = 10;
	 
	// BƯỚC 4: TÍNH TOÁN TOTAL_PAGE VÀ START
	// tổng số trang
	$total_page = ceil($total_records / $limit);
	 
	// Giới hạn current_page trong khoảng 1 đến total_page
	if ($current_page > $total_page){
	    $current_page = $total_page;
	}
	else if ($current_page < 1){
	    $current_page = 1;
	}
	 
	// Tìm Start
	$start = ($current_page - 1) * $limit;

	$result = $lbql -> query("SELECT * FROM `litebans_mutes` LIMIT $start, $limit");

	if ($result -> num_rows <= 0){
		echo "Không có User Bị Khoá Mõm";
	}else{
	?>

	<table>
		
		<thead>
			<tr>
				<th>Người Bị Cấm</th>
				<th>Người Cấm</th>
				<th>Người Bỏ Cấm</th>
				<th>Cấm Vào</th>
				<th>Ngày Hết Hạn</th>
				<th>Lý Do</th>
				<th>Máy Chủ Cấm</th>
				<th>Hiệu Lực</th>
			</tr>
		</thead>
		<tbody>
			<?php
				while ($a = $result -> fetch_array(MYSQLI_ASSOC)){
		
			?>
				<tr>
					<td><?=$a['uuid'];?></td>
					<td><?=$a['banned_by_name'];?></td>
					<td><?=$a['removed_by_name'];?></td>
					<td><?=date('H:i:s d/m/Y',$a['time']/1000);?></td>
					<td>
						<?php
							if ($a['until'] == -1){
								echo 'Vĩnh Viễn';
							}
						?>
					</td>
					<td><?=$a['reason'];?></td>
					<td><?=$a['server_origin'];?></td>
					<td>
						<?php
						if ($a['server_scope'] == "*"){
							echo "Toàn Network";
						}
						?>
					</td>
				</tr>

			<?php } ?>
		</tbody>
	</table>

	<?php } ?>
</div>
</div>
</div>