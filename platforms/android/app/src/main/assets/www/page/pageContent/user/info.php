<?php
$sql = $userql -> query("SELECT * FROM `authme` WHERE `realname` = '".$_SESSION['user']."' ");
$user  = $sql -> fetch_array(MYSQLI_ASSOC);

?>
<div class="container">
<div class="row">

	<div class="col s6">
	<div class="card light-blue lighten-5">
	<div class="card-content black-text">

		<div class="row">
			<div class="col s12">
				<div id="result-info"></div>
			</div>
		</div>

		<div class="row">
			<div class="input-field col s12">
				<input id="fullname" value="<?=$user['fullname'];?>" type="text" class="validate" required data-error="wrong" data-success="right">
				<label for="fullname">Tên Đầy Đủ</label>
			</div>
		</div>
		
		<div class="row">
			<div class="input-field col s12">
				<input id="username" value="<?=$user['realname'];?>" type="text" class="validate" disabled required data-error="wrong" data-success="right">
				<label for="username">Tên In-game</label>
			</div>
		</div>

		<div class="row">
			<div class="input-field col s12">
				<input id="email" value="<?=$user['email'];?>" type="email" class="validate" required data-error="wrong" data-success="right">
				<label for="email">Email (Bắt Buộc)</label>
			</div>
		</div>
		
		<div class="row">
			<div class="input-field col s12">
				<input id="phone" value="<?=$user['phone'];?>" type="number" class="validate" data-error="wrong" data-success="right">
				<label for="phone">Điện Thoại</label>
			</div>
		</div>

		<div class="row">
			<div class="input-field col s12">
				<input id="cmnd" value="<?=$user['cmnd'];?>" type="number" class="validate" data-error="wrong" data-success="right">
				<label for="cmnd">Chứng Minh Thư Nhân Dân</label>
			</div>
		</div>

		<div class="row">
			<div class="input-field col s12">
				<button class="btn waves-effect waves-light" onclick="updateInfo()" id="sendBtn" name="send">Cập Nhật 
					<i class="material-icons right">send</i>
				</button>
			</div>
		</div>

		<script type="text/javascript">
		function updateInfo(){

			name = $('#fullname').val();
			user = $('#username').val();
			email = $('#email').val();
			phone = $('#phone').val();
			cmnd = $('#cmnd').val();

			$.ajax({
				url : "<?=BASE_URL;?>/action/updateInfo.html",
				type : "post",
				dataType:"text",
				data : {
					fullname : name,
					username : user,
					email : email,
					phone : phone,
					cmnd : cmnd,
				},
				success : function (result){
					$('#result-info').html(result);
					
				}
			});
		}
		</script>
	
	</div>
	</div>
	</div>

	<div class="col s6">
	<div class="card light-green lighten-3">
	<div class="card-content black-text">

		<div class="row">
			<div class="col s12">
				<div id="result-pass"></div>
			</div>
		</div>

		<div class="row">
			<div class="input-field col s12">
				<input id="old-pass" type="password" class="validate" required>
				<label for="old-pass">Mật Khẩu Cũ</label>
			</div>
		</div>

		<div class="row">
			<div class="input-field col s12">
				<input id="new-pass" type="password" class="validate" required>
				<label for="new-pass">Mật Khẩu Mới</label>
			</div>
		</div>

		<div class="row">
			<div class="input-field col s12">
				<input id="new-pass-cf" type="password" class="validate" required>
				<label for="new-pass-cf">Nhập Lại Mật Khẩu Mới</label>
			</div>
		</div>

		<div class="row">
			<div class="input-field col s12">
				<button class="btn waves-effect waves-light" onclick="updatePassword()" id="sendBtn" name="send">Cập Nhật 
					<i class="material-icons right">send</i>
				</button>
			</div>
		</div>

		<script type="text/javascript">
		function updatePassword(){

			oldPass = $('#old-pass').val();
			newPass = $('#new-pass').val();
			newPassCF = $('#new-pass-cf').val();

			$.ajax({
				url : "<?=BASE_URL;?>/action/updatePassword.html",
				type : "post",
				dataType:"text",
				data : {
					oldPass : oldPass,
					newPass : newPass,
					newPassCF : newPassCF,
				},
				success : function (result){
					$('#result-pass').html(result);
				}
			});
		}
		</script>

	</div>
	</div>
	</div>

</div>
</div>