<div class="container">
<div class="card light-blue lighten-5">
<div class="card-content black-text">

<div class="row">
	<form class="col s12">
		<div class="row">
			<div class="input-field col s6">
				<input id="fullname" type="text" class="validate" required data-length="20">
				<label for="fullname">Tên Đầy Đủ Của Bạn</label>
			</div>
			
			<div class="input-field col s6">
				<input id="username" type="text" class="validate" required data-length="20">
				<label for="username">Tên In-game Của Bạn</label>
			</div>
		</div>
	
		<div class="row">
			<div class="input-field col s12">
				<input id="reported-ingame" type="text" class="validate" required data-length="20">
				<label for="reported-ingame">Tên In-game Người Bị Report</label>
			</div>
		</div>

		<div class="row">
			<div class="input-field col s6">
				<input id="day" type="text" class="datepicker">
				<label for="day">Ngày Phạm Lỗi</label>
			</div>
			<div class="input-field col s6">
				<input type="text" class="timepicker">
				<label for="time">Giờ Phạm Lỗi</label>

				<script>
					$('.timepicker').pickatime({
						default: 'now', // Set default time: 'now', '1:30AM', '16:30'
						fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
						twelvehour: false, // Use AM/PM or 24-hour format
						donetext: 'OK', // text for done-button
						cleartext: 'Clear', // text for clear-button
						canceltext: 'Cancel', // Text for cancel-button,
						container: undefined, // ex. 'body' will append picker to body
						autoclose: false, // automatic close timepicker
						ampmclickable: true, // make AM PM clickable
						aftershow: function(){} //Function for after opening timepicker
					});
				</script>
			</div>
		</div>
		
		<div class="row">
			<div class="input-field col s12">
				<textarea id="reason" class="materialize-textarea" required></textarea>
				<label for="reason">Lý Do Báo Cáo</label>
			</div>
		</div>
	
		<div class="row">
			<div class="file-field input-field">
				
				<div class="btn">
					<span>Bằng Chứng</span>
					<input type="file" multiple accept="image/x-png,image/gif,image/jpeg">
				</div>
				
				<div class="file-path-wrapper">
					<input class="file-path validate" type="text" placeholder="Chỉ Được Upload Ảnh !" accept="image/x-png,image/gif,image/jpeg">
				</div>
			
			</div>
		</div>

		<div class="row">
			<div class="input-field col s12">
				<textarea id="reason" placeholder="Upload Video Lên Youtube rồi dẫn link" class="materialize-textarea" required></textarea>
				<label for="reason">Link Video</label>
			</div>
		</div>

		<div class="row">
			<div class="input-field col s12 text-center">
				<button class="btn waves-effect waves-light" id="sendBtn" name="send">Gửi Đơn
					<i class="material-icons right">send</i>
				</button>
			</div>
		</div>

	</form>
</div>

</div>
</div>
</div>