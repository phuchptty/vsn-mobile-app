<div class="container">
<div class="card indigo darken-1">
<div class="card-content white-text">

	<table>
		<thead>
			<tr>
				<td></td>
				<td></td>
			</tr>
		</thead>

		<tbody>
			
			<tr>
				<td>Server Chính</td>
				<td>
				<?php
					$data = json_decode(curl_download("https://use.gameapis.net/mc/query/info/".IP_SERVER.":".PORT_BUNGEE), true);
					if ($data["status"] === true) {
						echo '<div class="green-text"><i class="material-icons">check</i> Online</div>';
					}else{
						echo '<div class="red-text"><i class="material-icons">close</i> Offline</div> Offline</div>';
					}
    			?>
				</td>
			</tr>

			<tr>
				<td>Server Lobby</td>
				<td>
				<?php
					$data = json_decode(curl_download("https://use.gameapis.net/mc/query/info/".IP_SERVER.":".PORT_LOBBY), true);
					if ($data["status"] === true) {
						echo '<div class="green-text"><i class="material-icons">check</i> Online</div>';
					}else{
						echo '<div class="red-text"><i class="material-icons">close</i> Offline</div>';
					}
    			?>
				</td>
			</tr>

			<tr>
				<td>Server Towny</td>
				<td>
				<?php
					$data = json_decode(curl_download("https://use.gameapis.net/mc/query/info/".IP_SERVER.":".PORT_TOWNY), true);
					if ($data["status"] === true) {
						echo '<div class="green-text"><i class="material-icons">check</i> Online</div>';
					}else{
						echo '<div class="red-text"><i class="material-icons">close</i> Offline</div>';
					}
    			?>
				</td>
			</tr>

			<tr>
				<td>Server Skyblock</td>
				<td><div class="red-text"><i class="material-icons">close</i> Offline</div></td>
			</tr>

			<?php
				$data = curl_download("https://status.mojang.com/check");
				$data = json_decode($data,true);
			?>

			<tr>
				<td>Server Trang Chủ Minecraft (Minecraft.net)</td>
				<td>
					<?php
					if ($data[0]["minecraft.net"] == "green") {
						echo '<div class="green-text"><i class="material-icons">check</i> Online</div>';
					}else if ($data[0]["minecraft.net"] == "yellow"){
						echo '<div class="yellow-text"><i class="material-icons">warning</i> Trục Trặc</div>';
					}else{
						echo '<div class="red-text"><i class="material-icons">close</i> Offline</div>';
					}

					?>
				</td>
			</tr>

			<tr>
				<td>Server Trang Chủ Mojang (mojang.com)</td>
				<td>
					<?php
					if ($data[7]["mojang.com"] == "green") {
						echo '<div class="green-text"><i class="material-icons">check</i> Online</div>';
					}else if ($data[7]["mojang.com"] == "yellow"){
						echo '<div class="yellow-text"><i class="material-icons">warning</i> Trục Trặc</div>';
					}else{
						echo '<div class="red-text"><i class="material-icons">close</i> Offline</div>';
					}

					?>
				</td>
			</tr>

			<tr>
				<td>Server Session Minecraft (session.minecraft.net)</td>
				<td>
					<?php
					if ($data[1]["session.minecraft.net"] == "green") {
						echo '<div class="green-text"><i class="material-icons">check</i> Online</div>';
					}else if ($data[1]["session.minecraft.net"] == "yellow"){
						echo '<div class="yellow-text"><i class="material-icons">warning</i> Trục Trặc</div>';
					}else{
						echo '<div class="red-text"><i class="material-icons">close</i> Offline</div>';
					}

					?>
				</td>
			</tr>

			<tr>
				<td>Server Account Minecraft (account.mojang.com)</td>
				<td>
					<?php
					if ($data[2]["account.mojang.com"] == "green") {
						echo '<div class="green-text"><i class="material-icons">check</i> Online</div>';
					}else if ($data[2]["account.mojang.com"] == "yellow"){
						echo '<div class="yellow-text"><i class="material-icons">warning</i> Trục Trặc</div>';
					}else{
						echo '<div class="red-text"><i class="material-icons">close</i> Offline</div>';
					}

					?>
				</td>
			</tr>

			<tr>
				<td>Authserver Minecraft (authserver.mojang.com)</td>
				<td>
					<?php
					if ($data[3]["authserver.mojang.com"] == "green") {
						echo '<div class="green-text"><i class="material-icons">check</i> Online</div>';
					}else if ($data[3]["authserver.mojang.com"] == "yellow"){
						echo '<div class="yellow-text"><i class="material-icons">warning</i> Trục Trặc</div>';
					}else{
						echo '<div class="red-text"><i class="material-icons">close</i> Offline</div>';
					}

					?>
				</td>
			</tr>

			<tr>
				<td>Sessionserver Minecraft (sessionserver.mojang.com)</td>
				<td>
					<?php
					if ($data[4]["sessionserver.mojang.com"] == "green") {
						echo '<div class="green-text"><i class="material-icons">check</i> Online</div>';
					}else if ($data[4]["sessionserver.mojang.com"] == "yellow"){
						echo '<div class="yellow-text"><i class="material-icons">warning</i> Trục Trặc</div>';
					}else{
						echo '<div class="red-text"><i class="material-icons">close</i> Offline</div>';
					}

					?>
				</td>
			</tr>

			<tr>
				<td>Server API Mojang (api.mojang.com)</td>
				<td>
					<?php
					if ($data[5]["api.mojang.com"] == "green") {
						echo '<div class="green-text"><i class="material-icons">check</i> Online</div>';
					}else if ($data[5]["api.mojang.com"] == "yellow"){
						echo '<div class="yellow-text"><i class="material-icons">warning</i> Trục Trặc</div>';
					}else{
						echo '<div class="red-text"><i class="material-icons">close</i> Offline</div>';
					}

					?>
				</td>
			</tr>

			<tr>
				<td>Server Texture Minecraft (textures.minecraft.net)</td>
				<td>
					<?php
					if ($data[6]["textures.minecraft.net"] == "green") {
						echo '<div class="green-text"><i class="material-icons">check</i> Online</div>';
					}else if ($data[6]["textures.minecraft.net"] == "yellow"){
						echo '<div class="yellow-text"><i class="material-icons">warning</i> Trục Trặc</div>';
					}else{
						echo '<div class="red-text"><i class="material-icons">close</i> Offline</div>';
					}

					?>
				</td>
			</tr>
		
		</tbody>
	</table>

	<div class="row">
		<div class="col s4 push-s8 ">
			Cập Nhật Lúc: <?=date('H:i:s d/m/Y');?>
		</div>

		<div class="col s8 pull-s4">
			<button class="waves-effect waves-light btn text-right" onclick="location.reload()"><i class="material-icons left">refresh</i> Tải Lại</button>
		</div>
	</div>

</div>
</div>
</div>